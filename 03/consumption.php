<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>消費税計算ページ</title>
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <h1>消費税計算</h1>
      <form method="post" action="consumption.php">
        <table border="1" cellspacing="0" style="margin-bottom:10px">
          <tbody>
            <tr>
              <th style="width:180px; height:30px;">
                商品名
              </th>
              <th style="width:180px; height:30px;">
                価格（単位：円、税別）
              </th>
              <th style="width:180px; height:30px;">
                個数
              </th>
              <th style="width:180px; height:30px;">
                税率
              </th>
            </tr>
            <tr>  <!--入力欄1-->
              <th style="width:180px; height:30px;">
                <input type="text" name="name1" id="name1" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="text" name="yen1" id="yen1" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="text" name="number1" id="number1" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="radio" name="consumption1" id="consumption1" value="8" checked="checked" style="padding-left: 10px;">8%
                <input type="radio" name="consumption1" id="consumption1" value="10">10%
              </th>
            </tr>
            <tr>  <!--入力欄2-->
              <th style="width:180px; height:30px;">
                <input type="text" name="name2" id="name2" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="text" name="yen2" id="yen2" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="text" name="number2" id="number2" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="radio" name="consumption2" id="consumption2" value="8" checked="checked" style="padding-left: 10px;">8%
                <input type="radio" name="consumption2" id="consumption2" value="10">10%
              </th>
            </tr>
            <tr>  <!--入力欄3-->
              <th style="width:180px; height:30px;">
                <input type="text" name="name3" id="name3" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="text" name="yen3" id="yen3" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="text" name="number3" id="number3" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="radio" name="consumption3" id="consumption3" value="8" checked="checked" style="padding-left: 10px;">8%
                <input type="radio" name="consumption3" id="consumption3" value="10">10%
              </th>
            </tr>
            <tr>  <!--入力欄4-->
              <th style="width:180px; height:30px;">
                <input type="text" name="name4" id="name4" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="text" name="yen4" id="yen4" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="text" name="number4" id="number4" value="" />
              </th>
              <th style="width:180px; height:30px;">
                <input type="radio" name="consumption4" id="consumption4" value="8" checked="checked" style="padding-left: 10px;">8%
                <input type="radio" name="consumption4" id="consumption4" value="10">10%
              </th>
            </tr>
          </tbody>
        </table>

        <div style="margin-bottom:40px;">
          <button type="submit" name="agree" value="送信">送信
          <button type="reset" name="clear" value="取り消し">取り消し
        </div>

      </form>

      <h1>計算結果出力欄</h1>
      <p>小数点以下1桁四捨五入</p>
      <table border="1" cellspacing="0"> <!--表示欄-->
        <tbody>
          <tr>
            <th style="width:180px; height:30px;">
              商品名
            </th>
            <th style="width:180px; height:30px;">
              価格（単位：円、税別）
            </th>
            <th style="width:180px; height:30px;">
              個数
            </th>
            <th style="width:180px; height:30px;">
              税率
            </th>
            <th style="width:180px; height:30px;">
              小計（単位：円、税込）
            </th>
          </tr>
          <tr>  <!--入力欄1-->
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['name1']) && $_POST['name1'] != ""){
                  echo $_POST['name1'];
                }
              ?><!--isset文で「変数が入力されている」かつ、!=""で「対象が空欄でない時」にechoを返し、そうでない時は空欄にする-->
            </th>
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['yen1']) && $_POST['yen1'] != ""){
                  $yen1 = $_POST['yen1'];
                  $yen1 = round ($yen1,0);
                  echo number_format($yen1) . "円";
                }
              ?>
            </th>
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['number1']) && $_POST['number1'] != ""){
                  $number1 = $_POST['number1'];
                  $number1 = round ($number1,0);
                  echo number_format($number1) . "個";
                }
              ?>
            </th>
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['consumption1']) && $_POST['consumption1'] != ""){
                  echo $_POST['consumption1'] . "%";
                }
              ?>
            </th>
            <th style="width:180px; height:30px; text-align:right;">
              <?php
                if(isset($_POST['number1']) && $_POST['number1'] != ""){
                  $consumption1 = $_POST['consumption1'];
                  $consumption1 = $consumption1 * 0.01 + 1;
                  $price1 = $yen1 * $number1 * $consumption1;
                  echo number_format(round ($price1,0)) . "円";
                }
              ?><!--消費税を直接consumption.idに入れられなかったので、consumptionを呼び出して0.01倍して+1した-->
            </th>
          </tr>
          <tr>  <!--入力欄2-->
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['name2']) && $_POST['name2'] != ""){
                  echo $_POST['name2'];
                }
              ?>
            </th>
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['yen2']) && $_POST['yen2'] != ""){
                  $yen2 = $_POST['yen2'];
                  $yen2 = round ($yen2,0);
                  echo number_format($yen2) . "円";
                }
              ?>
            </th>
            <th>
              <?php
                if(isset($_POST['number2']) && $_POST['number2'] != ""){
                  $number2 = $_POST['number2'];
                  $number2 = round ($number2,0);
                  echo number_format($number2) . "個";
                }
              ?>
            </th>
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['consumption2']) && $_POST['consumption2'] != ""){
                  echo $_POST['consumption2'] . "%";
                }
              ?>
            </th>
            <th style="width:180px; height:30px; text-align:right;">
              <?php
                if(isset($_POST['number2']) && $_POST['number2'] != ""){
                  $consumption2 = $_POST['consumption2'];
                  $consumption2 = $consumption2 * 0.01 + 1;
                  $price2 = $yen2 * $number2 * $consumption2;
                  echo number_format(round ($price2,0)) . "円";
                }
              ?>
            </th>
          </tr>
          <tr>  <!--入力欄3-->
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['name3']) && $_POST['name3'] != ""){
                  echo $_POST['name3'];
                }
              ?>
            </th>
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['yen3']) && $_POST['yen3'] != ""){
                  $yen3 = $_POST['yen3'];
                  $yen3 = round ($yen3,0);
                  echo number_format($yen3) . "円";
                }
              ?>
            </th>
            <th>
              <?php
                if(isset($_POST['number3']) && $_POST['number3'] != ""){
                  $number3 = $_POST['number3'];
                  $number3 = round ($number3,0);
                  echo number_format($number3) . "個";
                }
              ?>
            </th>
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['consumption3']) && $_POST['consumption3'] != ""){
                  echo $_POST['consumption3'] . "%";
                }
              ?>
            </th>
            <th style="width:180px; height:30px; text-align:right;">
              <?php
                if(isset($_POST['number3']) && $_POST['number3'] != ""){
                  $consumption3 = $_POST['consumption3'];
                  $consumption3 = $consumption3 * 0.01 + 1;
                  $price3 = $yen3 * $number3 * $consumption3;
                  echo number_format(round ($price3,0)) . "円";
                }
              ?>
            </th>
          </tr>
          <tr>  <!--入力欄4-->
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['name4']) && $_POST['name4'] != ""){
                  echo $_POST['name4'];
                }
              ?>
            </th>
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['yen4']) && $_POST['yen4'] != ""){
                  $yen4 = $_POST['yen4'];
                  $yen4 = round ($yen4,0);
                  echo number_format($yen4) . "円";
                }
              ?>
            </th>
            <th>
              <?php
                if(isset($_POST['number4']) && $_POST['number4'] != ""){
                  $number4 = $_POST['number4'];
                  $number4 = round ($number4,0);
                  echo number_format($number4) . "個";
                }
              ?>
            </th>
            <th style="width:180px; height:30px;">
              <?php
                if(isset($_POST['consumption4']) && $_POST['consumption4'] != ""){
                  echo $_POST['consumption4'] . "%";
                }
              ?>
            </th>
            <th style="width:180px; height:30px; text-align:right;">
              <?php
                if(isset($_POST['number4']) && $_POST['number4'] != ""){
                  $consumption4 = $_POST['consumption4'];
                  $consumption4 = $consumption4 * 0.01 + 1;
                  $price4 = $yen4 * $number4 * $consumption4;
                  echo number_format(round ($price4,0)) . "円";
                }
              ?>
            </th>
          </tr>
          <tr>
            <th colspan="4">
              合計
            </th>
            <th style="text-align:right;">
              <?php
                if(isset($_POST['number1']) && $_POST['number1'] != ""){
                  $priceall = $price1 + $price2 + $price3 + $price4;
                  echo number_format(round ($priceall,0)) . "円";
                }
              ?>
            </th>
          </tr>
        </tbody>
      </table>
  </body>
</html>
