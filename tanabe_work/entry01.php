<!DOCTYPE html>
<link rel="stylesheet" hret="./include/style.css">
<link rel="stylesheet" hret="./style.css">
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>新規社員登録</title>

    <script type="text/javascript">
      function back(){
        location.href='http://192.168.11.152/work/tanabe_work/index.php';
      }
      function check(){
        var result = window.confirm('この内容で登録しますか？');
        if(result){
          document.nw_form.submit();
        } else{
          return false;
        }
      }
    </script>

  </head>
  <body style="padding:0 10px;">
    <div class="container w-80">
    <div id="header">
      <?php include($_SERVER['DOCUMENT_ROOT'] . './work/tanabe_work/header.php'); ?>
    </div>

    <form name="nw_form" method="POST" action="entry02.php">
      <table class="table table-striped table-bordered table-condensed">
        <tr class="form-group">
          <th>
            <div>名前</div>
          </th>
          <td>
            <label>
              <input type="text" class="form-control" name="nw_name" id="nw_name" value="" placeholder="社員名" />
            </label>
          </td>
        </tr>
        <tr class="form-group">
          <th>
            <div>出身地</div>
          </th>
          <td>
            <select class="form-control" name="nw_area" id="nw_area" style="width: 120px;">
              <option value="" selected>都道府県</option>
              <option value="北海道">北海道</option><option value="青森県">青森県</option><option value="岩手県">岩手県</option><option value="宮城県">宮城県</option><option value="秋田県">秋田県</option><option value="山形県">山形県</option><option value="福島県">福島県</option><option value="茨城県">茨城県</option><option value="栃木県">栃木県</option><option value="群馬県">群馬県</option><option value="埼玉県">埼玉県</option><option value="千葉県">千葉県</option><option value="東京都">東京都</option><option value="神奈川県">神奈川県</option><option value="新潟県">新潟県</option><option value="富山県">富山県</option><option value="石川県">石川県</option><option value="福井県">福井県</option><option value="山梨県">山梨県</option><option value="長野県">長野県</option><option value="岐阜県">岐阜県</option><option value="静岡県">静岡県</option><option value="愛知県">愛知県</option><option value="三重県">三重県</option><option value="滋賀県">滋賀県</option><option value="京都府">京都府</option><option value="大阪府">大阪府</option><option value="兵庫県">兵庫県</option><option value="奈良県">奈良県</option><option value="和歌山県">和歌山県</option><option value="鳥取県">鳥取県</option><option value="島根県">島根県</option><option value="岡山県">岡山県</option><option value="広島県">広島県</option><option value="山口県">山口県</option><option value="徳島県">徳島県</option><option value="香川県">香川県</option><option value="愛媛県">愛媛県</option><option value="高知県">高知県</option><option value="福岡県">福岡県</option><option value="佐賀県">佐賀県</option><option value="長崎県">長崎県</option><option value="熊本県">熊本県</option><option value="大分県">大分県</option><option value="宮崎県">宮崎県</option><option value="鹿児島県">鹿児島県</option><option value="沖縄県">沖縄県</option>
            </select>
          </td>
        </tr>
        <tr class="form-group">
          <th>
            <div>性別</div>
          </th>
          <td>
            <label>
              <input type="radio" name="nw_sex" id="nw_sex" value="男" checked />男
              <input type="radio" name="nw_sex" id="nw_sex" value="女" />女
            </label>
          </td>
        </tr>
        <tr class="form-group">
          <th>
            <div>年齢</div>
          </th>
          <td>
            <label>
              <input type="text" class="form-control" name="nw_age" id="nw_age" value="" placeholder="年齢（半角数字）" />
            </label>
          </td>
        </tr>
        <tr class="form-group">
          <th>
            <div>部署</div>
          </th>
          <td>
            <label>
              <input type="radio" name="nw_section" id="nw_section" value="1" checked />第一事業本部
              <input type="radio" name="nw_section" id="nw_section" value="2" />第二事業本部
              <input type="radio" name="nw_section" id="nw_section" value="3" />営業
              <input type="radio" name="nw_section" id="nw_section" value="4" />総務
              <input type="radio" name="nw_section" id="nw_section" value="5" />人事
            </label>
          </td>
        </tr>
        <tr class="form-group">
          <th>
            <div>役職</div>
          </th>
          <td>
            <label>
              <input type="radio" name="nw_grade" id="nw_grade" value="1" checked />事業部長
              <input type="radio" name="nw_grade" id="nw_grade" value="2" />部長
              <input type="radio" name="nw_grade" id="nw_grade" value="3" />チームリーダー
              <input type="radio" name="nw_grade" id="nw_grade" value="4" />リーダー
              <input type="radio" name="nw_grade" id="nw_grade" value="5" />メンバー
            </label>
          </td>
        </tr>
        <tr class="form-group">
          <th>
            <div>備考</div>
          </th>
          <td>
            <label>
              <textarea class="form-control" name="nw_other" id="nw_other" value="" placeholder="その他備考を入力します。" style="width: 400px;" rows="3"></textarea>
            </label>
          </td>
        </tr>
      </table>
      <div class="form-group" style="text-align: center;">
        <button type="button" name="update" id="update" onclick="return back();" class="btn btn-success">TOP</button>
        <button type="button" name="search" id="search" value="" onclick="return check();" class="btn btn-primary">新規登録</button>
        <button type="reset" name="reset" value="" class="btn btn-warning">入力リセット</button>
      </div>
    </form>

  </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
