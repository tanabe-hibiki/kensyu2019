<!DOCTYPE html>
<link rel="stylesheet" hret="./include/style.css">
<link rel="stylesheet" hret="style.css">
<?php
  // common
  // include(./include/fanctions.php);
  $DB_DSN = "mysql:host=localhost; dbname=kensyu_tanabe; charset=utf8";
  $DB_USER = "tanabe";
  $DB_PW = "WUSHKMNXSzcBfhUP";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  // $pdo=initDB();

  if(isset($_GET['id']) && $_GET['id'] != ""){
    $query_str =
      "SELECT
        m.id,
        m.name,
        m.area,
        m.sex,
        m.age,
        m.section_id,
        sm.name as 'sm.name',
        m.grade_id,
        gm.name as 'gm.name',
        m.other
      FROM
        member AS m
          LEFT JOIN section_master AS sm ON sm.id = m.section_id
          LEFT JOIN grade_master AS gm ON gm.id = m.grade_id
      WHERE
        m.id='" . $_GET['id'] . "'
      ";

    $sql = $pdo->prepare($query_str);
    $sql -> execute();
    $result = $sql -> fetchAll();
    //echo $query_str;
    if(count($result) == 0){
      echo "<div class='text-center'>存在しないIDです。<br />
            <a href='index.php'>トップページ</a>に戻る。</div>";
    }
  } else{
    echo "<div class='text-center'>IDが入力されていません。<br />
          <a href='index.php'>トップページ</a>に戻る。</div>";
  };
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>社員情報更新</title>

    <script type="text/javascript">
      function check(){
        var result = window.confirm('この内容で更新しますか？');
        if(result){
          document.upd_form.submit();
        } else{
          return false;
        }
      }
    </script>

  </head>
  <body style="padding:0 10px;">
    <div class="container w-80">
      <div id="header">
        <?php include($_SERVER['DOCUMENT_ROOT'] . './work/tanabe_work/header.php'); ?>
      </div>
    <?php if(isset($_GET['id']) && $_GET['id'] != "" && count($result) != 0){ ?>
      <form name="upd_form" method="POST" action="entry_update02.php">
        <table class="table table-striped table-bordered">
          <tr class="form-group">
            <th>ID</th>
            <td>
              <label>
                <input type="hidden" name="id" id ="id" value="<?php echo $result[0]['id'] ?>">
                <?php echo $result[0]['id'] ?>
              </label>
            </td>
          </tr>
          <tr class="form-group">
            <th><div>社員名</div></th>
            <td>
              <label>
                <input type="text" class="form-control" name="upd_name" id="upd_name" value="<?php echo $result[0]['name'] ?>">
              </label>
            </td>
          </tr>
          <tr class="form-group">
            <th>出身地</th>
            <td>
              <select class="form-control" name="upd_area" id="upd_area" style="width: 120px;">
                <option <?php if ($result[0]['area'] == "") { echo "selected"; } ?> value="">都道府県</option>
                <option <?php if ($result[0]['area'] == "北海道") { echo "selected"; } ?> value="北海道">北海道</option>
                <option <?php if ($result[0]['area'] == "青森県") { echo "selected"; } ?> value="青森県">青森県</option>
                <option <?php if ($result[0]['area'] == "岩手県") { echo "selected"; } ?> value="岩手県">岩手県</option>
                <option <?php if ($result[0]['area'] == "宮城県") { echo "selected"; } ?> value="宮城県">宮城県</option>
                <option <?php if ($result[0]['area'] == "秋田県") { echo "selected"; } ?> value="秋田県">秋田県</option>
                <option <?php if ($result[0]['area'] == "山形県") { echo "selected"; } ?> value="山形県">山形県</option>
                <option <?php if ($result[0]['area'] == "福島県") { echo "selected"; } ?> value="福島県">福島県</option>
                <option <?php if ($result[0]['area'] == "茨城県") { echo "selected"; } ?> value="茨城県">茨城県</option>
                <option <?php if ($result[0]['area'] == "栃木県") { echo "selected"; } ?> value="栃木県">栃木県</option>
                <option <?php if ($result[0]['area'] == "新潟県") { echo "selected"; } ?> value="新潟県">新潟県</option>
                <option <?php if ($result[0]['area'] == "埼玉県") { echo "selected"; } ?> value="埼玉県">埼玉県</option>
                <option <?php if ($result[0]['area'] == "千葉県") { echo "selected"; } ?> value="千葉県">千葉県</option>
                <option <?php if ($result[0]['area'] == "東京都") { echo "selected"; } ?> value="東京都">東京都</option>
                <option <?php if ($result[0]['area'] == "神奈川県") { echo "selected"; } ?> value="神奈川県">神奈川県</option>
                <option <?php if ($result[0]['area'] == "富山県") { echo "selected"; } ?> value="富山県">富山県</option>
                <option <?php if ($result[0]['area'] == "石川県") { echo "selected"; } ?> value="石川県">石川県</option>
                <option <?php if ($result[0]['area'] == "福井県") { echo "selected"; } ?> value="福井県">福井県</option>
                <option <?php if ($result[0]['area'] == "山梨県") { echo "selected"; } ?> value="山梨県">山梨県</option>
                <option <?php if ($result[0]['area'] == "長野県") { echo "selected"; } ?> value="長野県">長野県</option>
                <option <?php if ($result[0]['area'] == "岐阜県") { echo "selected"; } ?> value="岐阜県">岐阜県</option>
                <option <?php if ($result[0]['area'] == "静岡県") { echo "selected"; } ?> value="静岡県">静岡県</option>
                <option <?php if ($result[0]['area'] == "愛知県") { echo "selected"; } ?> value="愛知県">愛知県</option>
                <option <?php if ($result[0]['area'] == "三重県") { echo "selected"; } ?> value="三重県">三重県</option>
                <option <?php if ($result[0]['area'] == "滋賀県") { echo "selected"; } ?> value="滋賀県">滋賀県</option>
                <option <?php if ($result[0]['area'] == "京都府") { echo "selected"; } ?> value="京都府">京都府</option>
                <option <?php if ($result[0]['area'] == "大阪府") { echo "selected"; } ?> value="大阪府">大阪府</option>
                <option <?php if ($result[0]['area'] == "兵庫県") { echo "selected"; } ?> value="兵庫県">兵庫県</option>
                <option <?php if ($result[0]['area'] == "奈良県") { echo "selected"; } ?> value="奈良県">奈良県</option>
                <option <?php if ($result[0]['area'] == "和歌山県") { echo "selected"; } ?> value="和歌山県">和歌山県</option>
                <option <?php if ($result[0]['area'] == "鳥取県") { echo "selected"; } ?> value="鳥取県">鳥取県</option>
                <option <?php if ($result[0]['area'] == "島根県") { echo "selected"; } ?> value="島根県">島根県</option>
                <option <?php if ($result[0]['area'] == "岡山県") { echo "selected"; } ?> value="岡山県">岡山県</option>
                <option <?php if ($result[0]['area'] == "広島県") { echo "selected"; } ?> value="広島県">広島県</option>
                <option <?php if ($result[0]['area'] == "山口県") { echo "selected"; } ?> value="山口県">山口県</option>
                <option <?php if ($result[0]['area'] == "徳島県") { echo "selected"; } ?> value="徳島県">徳島県</option>
                <option <?php if ($result[0]['area'] == "香川県") { echo "selected"; } ?> value="香川県">香川県</option>
                <option <?php if ($result[0]['area'] == "愛媛県") { echo "selected"; } ?> value="愛媛県">愛媛県</option>
                <option <?php if ($result[0]['area'] == "高知県") { echo "selected"; } ?> value="高知県">高知県</option>
                <option <?php if ($result[0]['area'] == "福岡県") { echo "selected"; } ?> value="福岡県">福岡県</option>
                <option <?php if ($result[0]['area'] == "佐賀県") { echo "selected"; } ?> value="佐賀県">佐賀県</option>
                <option <?php if ($result[0]['area'] == "長崎県") { echo "selected"; } ?> value="長崎県">長崎県</option>
                <option <?php if ($result[0]['area'] == "熊本県") { echo "selected"; } ?> value="熊本県">熊本県</option>
                <option <?php if ($result[0]['area'] == "大分県") { echo "selected"; } ?> value="大分県">大分県</option>
                <option <?php if ($result[0]['area'] == "宮崎県") { echo "selected"; } ?> value="宮崎県">宮崎県</option>
                <option <?php if ($result[0]['area'] == "鹿児島県") { echo "selected"; } ?> value="鹿児島県">鹿児島県</option>
                <option <?php if ($result[0]['area'] == "沖縄県") { echo "selected"; } ?> value="沖縄県">沖縄県</option>
              </select>
            </td>
          </tr>
          <tr class="form-group">
            <th><div>性別</div></th>
            <td>
              <label>
                <?php
                  if($result[0]['sex'] == "男"){
                    echo '<input type="radio" name="upd_sex" id="upd_sex" value="男" checked />男
                          <input type="radio" name="upd_sex" id="upd_sex" value="女">女';
                  } else{
                    echo '<input type="radio" name="upd_sex" id="upd_sex" value="男" />男
                          <input type="radio" name="upd_sex" id="upd_sex" value="女" checked />女';
                  };
                ?>
              </label>
            </td>
          </tr>
          <tr class="form-group">
            <th><div>年齢</div></th>
            <td>
              <label>
                <input type="text" class="form-control" name="upd_age" id="upd_age" value="<?php echo $result[0]['age'] ?>" style="width: 45px;">
              </label>
            </td>
          </tr>
          <tr class="form-group">
            <th>所属</th>
            <td>
              <label>
                <input type="radio" name="upd_section_id" id="upd_section_id" value="1" <?php if($result[0]['section_id'] == "1"){echo 'checked';} ?> />第一事業本部
                <input type="radio" name="upd_section_id" id="upd_section_id" value="2" <?php if($result[0]['section_id'] == "2"){echo 'checked';} ?> />第二事業本部
                <input type="radio" name="upd_section_id" id="upd_section_id" value="3" <?php if($result[0]['section_id'] == "3"){echo 'checked';} ?> />営業
                <input type="radio" name="upd_section_id" id="upd_section_id" value="4" <?php if($result[0]['section_id'] == "4"){echo 'checked';} ?> />総務
                <input type="radio" name="upd_section_id" id="upd_section_id" value="5" <?php if($result[0]['section_id'] == "5"){echo 'checked';} ?> />人事
              </label>
            </td>
          </tr>
          <tr class="form-group"><th>役職</th>
            <td>
              <input type="radio" name="upd_grade_id" id="upd_grade_id" value="1" <?php if($result[0]['grade_id'] == "1"){echo 'checked';} ?> />事業部長
              <input type="radio" name="upd_grade_id" id="upd_grade_id" value="2" <?php if($result[0]['grade_id'] == "2"){echo 'checked';} ?> />部長
              <input type="radio" name="upd_grade_id" id="upd_grade_id" value="3" <?php if($result[0]['grade_id'] == "3"){echo 'checked';} ?> />チームリーダー
              <input type="radio" name="upd_grade_id" id="upd_grade_id" value="4" <?php if($result[0]['grade_id'] == "4"){echo 'checked';} ?> />リーダー
              <input type="radio" name="upd_grade_id" id="upd_grade_id" value="5" <?php if($result[0]['grade_id'] == "5"){echo 'checked';} ?> />メンバー
            </td>
          </tr>
          <tr class="form-group">
            <th><div>備考<br />※必須ではありません</div></th>
            <td>
              <label>
                <textarea class="form-control" name="upd_other" id="upd_other" style="width: 400px;" rows="3"><?php echo $result[0]['other'] ?></textarea>
              </label>
            </td>
          </tr>
        </table>
        <div class="form-group" style="text-align: center;">
          <button type="button" name="upd_btn" id="upd_btn" value="" onclick="return check();" class="btn btn-primary">データ更新</button>
          <button type="reset" name="reset" value="" class="btn btn-warning">入力リセット</button>
        </div>
      </form>
    </div>
  <?php }; ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
