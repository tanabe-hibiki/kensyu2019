<!DOCTYPE html>
<link rel="stylesheet" hret="./include/style.css">
<link rel="stylesheet" hret="./style.css">
<?php
  // common
  // include(./include/fanctions.php);
  $DB_DSN = "mysql:host=localhost; dbname=kensyu_tanabe; charset=utf8";
  $DB_USER = "tanabe";
  $DB_PW = "WUSHKMNXSzcBfhUP";
  $pdo = new PDO($DB_DSN, $DB_USER, $DB_PW);
  // $pdo=initDB();

  $where_str = "";
  $query_str =
    "SELECT
      m.id,
      m.name,
      m.area,
      m.sex,
      m.age,
      m.section_id,
      sm.name as 'sm.name',
      m.grade_id,
      gm.name as 'gm.name',
      m.other
    FROM
      member AS m
        LEFT JOIN section_master AS sm ON sm.id = m.section_id
        LEFT JOIN grade_master as gm on gm.id = m.grade_id
    ";

    if(!empty($_GET['m_name']) == "" && !empty($_GET['sex']) == "" && !empty($_GET['section']) == "" && !empty($_GET['grade']) == ""){
      $where_str .= "";
    }
    else if(isset($_GET['m_name']) || isset($_GET['sex']) || isset($_GET['section']) || isset($_GET['grade'])){
      $where_str .=
        "WHERE ";
      if($_GET['m_name'] != ""){
        $where_str .=
        "m.name like '" . '%' . $_GET['m_name'] . '%' . "' AND ";
      }
      if($_GET['sex'] != ""){
        if($_GET['sex'] == 1){
          $where_str .=
            "m.sex = '男' AND ";
        } else if($_GET['sex'] == 2){
          $where_str .=
            "m.sex = '女' AND ";
        }
      }
      if($_GET['section'] != ""){
        $where_str .=
          "m.section_id = " . $_GET['section'] . " AND ";
      }
      if($_GET['grade'] != ""){
        $where_str .=
          "m.grade_id = " . $_GET['grade'] . " AND ";
      }
    }

  $cut = -4;
  $where_str = substr($where_str, 0 , $cut);
  $query_str.=$where_str;
  $sql = $pdo->prepare($query_str);
  $sql -> execute();
  $result = $sql -> fetchAll();
  //echo $query_str;
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>社員検索</title>

    <script type="text/javascript">
      function clearind(){
        document.getElementById("m_name").value="";
        document.getElementById("sex").value="";
        document.getElementById("section").value="";
        document.getElementById("grade").value="";
      }
    </script>

  </head>
  <body style="padding:0 10px;">
    <div class="container w-80">
      <div id="header">
        <?php include($_SERVER['DOCUMENT_ROOT'] . './work/tanabe_work/header.php'); ?>
      </div>

      <form name="check" method="get" action="index.php" >
        <div>
          <div class="form-group form-inline">
            名前：<input type="text" class="form-control" name="m_name" id="m_name" value="<?php if(!empty($_GET['m_name'])){echo $_GET['m_name'];} ?>" placeholder="社員名（部分一致）" />
          </div>
          <div class="form-group form-inline">
            性別：<select name="sex" id="sex" class="form-control" style="width: 120px; margin-right:10px;">
              <option value="" <?php if(!empty($_GET['sex']) && $_GET['sex'] === ""){echo 'selected';} ?>>すべて</option>
              <option value="1" <?php if(!empty($_GET['sex']) && $_GET['sex'] === "1"){echo 'selected';} ?>>男性</option>
              <option value="2" <?php if(!empty($_GET['sex']) && $_GET['sex'] === "2"){echo 'selected';} ?>>女性</option></select>
            部署：<select name="section" id="section" class="form-control" style="width: 180px; margin-right:10px;">
              <option value="" <?php if(!empty($_GET['section']) && $_GET['section'] === ""){echo 'selected';} ?>>すべて</option>
              <option value="1" <?php if(!empty($_GET['section']) && $_GET['section'] === "1"){echo 'selected';} ?>>第一事業本部</option>
              <option value="2" <?php if(!empty($_GET['section']) && $_GET['section'] === "2"){echo 'selected';} ?>>第二事業本部</option>
              <option value="3" <?php if(!empty($_GET['section']) && $_GET['section'] === "3"){echo 'selected';} ?>>営業</option>
              <option value="4" <?php if(!empty($_GET['section']) && $_GET['section'] === "4"){echo 'selected';} ?>>総務</option>
              <option value="5" <?php if(!empty($_GET['section']) && $_GET['section'] === "5"){echo 'selected';} ?>>人事</option></select>
            役職：<select name="grade" id="grade" class="form-control" style="width: 180px;">
              <option value="" <?php if(!empty($_GET['grade']) && $_GET['grade'] === ""){echo 'selected';} ?>>すべて</option>
              <option value="1" <?php if(!empty($_GET['grade']) && $_GET['grade'] === "1"){echo 'selected';} ?>>事業部長</option>
              <option value="2" <?php if(!empty($_GET['grade']) && $_GET['grade'] === "2"){echo 'selected';} ?>>部長</option>
              <option value="3" <?php if(!empty($_GET['grade']) && $_GET['grade'] === "3"){echo 'selected';} ?>>チームリーダー</option>
              <option value="4" <?php if(!empty($_GET['grade']) && $_GET['grade'] === "4"){echo 'selected';} ?>>リーダー</option>
              <option value="5" <?php if(!empty($_GET['grade']) && $_GET['grade'] === "5"){echo 'selected';} ?>>メンバー</option></select>
          </div>
          <div class="form-group" style="text-align: center;">
            <button type="submit" name="search" value="" class="btn btn-primary">検索</button>
            <button type="button" name="clear" id="clear" onclick="return clearind();" class="btn btn-warning">リセットする</button>
          </div>
        </div>
      </form>

      <hr />

      <?php
        echo "<p>検索結果は " . count($result) . " 件です。</p>";
        echo '<table class="table table-striped table-bordered table-hover table-condensed">';
        echo '<tr><td>ID</td><td>社員名</td><td>出身地</td><td>性別</td><td>年齢</td><td>所属</td><td>役職</td></tr>';
        foreach($result as $each){
          // var_dump($each);
          echo '<tr>';
          echo '<td style="width: 50px;">' . $each['id'] . '</td>' .
               '<td style="width: 120px;"><a href="detail.php?id=' . $each['id'] . '">' . $each['name'] . '</a></td>' .
               '<td style="width: 120px;">' . $each['area'] . '</td>' .
               '<td style="width: 50px;">' . $each['sex'] . '</td>' .
               '<td style="width: 50px;">' . $each['age'] . '</td>' .
               '<td style="width: 120px;">' . $each['sm.name'] . '</td>' .
               '<td style="width: 120px;">' . $each['gm.name'] . '</td>';
          echo '</tr>';
        } echo '</table>';
      ?>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
