<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>指定行テーブル生成</title>
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
    <h1>指定行テーブル生成</h1>
    <form method="post" action="generation-table-line.php">
      <div>
        <input type="text" name="line" id="line" value="" placeholder="1以上の整数を入力" />行のテーブルを生成する。
      </div>

      <div style="margin-top:30px;">
        <button type="submit" name="gene" value="生成">生成
        <button type="reset" name="clear" value="クリア">クリア
      </div>
    </form>

    <hr>


      <?php
        if(isset($_POST['line']) && $_POST['line'] != "" && $_POST['line'] >= 1){
          echo '<table border="1" cellspacing="0">';
          for($i=0; $i<$_POST['line']; $i++){
            echo '<tr>';
            echo '<td>テスト1</td>';
            echo '<td>テスト2</td>';
            echo '<td>テスト3</td>';
            echo '</tr>';
          }
          echo '</table>';
        }
        else{
            echo "1以上の整数を入力してください。";
        }
      ?>
  </body>
</html>
