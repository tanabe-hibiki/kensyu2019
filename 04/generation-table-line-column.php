<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>指定行列テーブル生成</title>
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
    <h1>指定行列テーブル生成</h1>
    <form method="post" action="generation-table-line-column.php">
      <div>
        <input type="text" name="line" id="line" value="" placeholder="行" style="width:40px;" />行 ×
        <input type="text" name="column" id="column" value="" placeholder="列" style="width:40px;" />列のテーブルを生成する。
      </div>

      <div style="margin-top:30px;">
        <button type="submit" name="gene" value="生成">生成
        <button type="reset" name="clear" value="クリア">クリア
      </div>
    </form>

    <hr>

    <?php
      if(isset($_POST['line']) && $_POST['line'] != "" && $_POST['line'] >= 1){
        if(isset($_POST['column']) && $_POST['column'] != "" && $_POST['column'] >= 1){
          echo '<table border="1" cellspacing="0">';
          for($i=1; $i<$_POST['line']+1; $i++){
            echo '<tr>';
            for($j=1; $j<$_POST['column']+1; $j++){
              echo '<td>' . $i . '-' . $j . '</td>';
            } echo '</tr>';
          }
          echo '</table>';
        } else{
          echo "1以上の整数を入力してください。";
        }
      }
    ?>

  </body>
</html>
