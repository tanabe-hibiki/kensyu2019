<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ログインフォーム処理</title>
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
    <h1>ログインフォーム</h1>
    <form method="post" action="result.php">
      <div>
        <label class="col-sm-4">
          ログインID
        </label>
        <div class="col-sm-8">
          <input type="text" name="id" id="id" value="" placeholder="ログインIDを入力してください" />
        </div>
      </div>

      <div>
        <label>
          パスワード
        </label>
        <div>
          <input type="password" name="pw" id="pw" value="" placeholder="パスワードを入力してください" />
        </div>
      </div>

      <div style="margin-top:30px;">
        <button type="submit" name="login" value="ログイン">ログイン
      </div>
    </form>
  </body>
</html>
